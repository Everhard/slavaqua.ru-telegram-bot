<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Telegram Bot</title>

    <!-- Bootstrap -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <style type="text/css">
	body {
		padding-top: 50px;
	}
	#loader {display: none; height: 30px; width: 30px;}
    </style>
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<script type="text/javascript">
	$(document).ready(function() {
		$("#send").click(function() {
			var priceType = ($("#price_type").prop('checked')) ? 1 : 0;
			$.ajax({
				method: "POST",
				url: "ajax.php",
				data: {
					message: $("#part_number").val()
				},
				success: function(response) {
					$("#loader").hide();
					$("#answer_window").prepend("<div style='display: none' class=\"answer\">" + response + "</div>");
					$("#answer_window .answer").fadeIn(500);
						
				},
				beforeSend: function() {
					if (!$("#part_number").val()) return false;
					
					$("#loader").show();
					$("#answer_window .answer").remove();
					$("#part_number").val('');
				}
			});
			return false;
		});
	});
</script>
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body>
<div class="container">
    <div class="jumbotron">
        <h1>Telegram bot</h1>
     
<div class="well">
<pre id="answer_window" style="padding: 20px; background: #f7f7f9; color: #2f6f9f; height: 300px;">

</pre>
<form class="form-inline">
  <div class="form-group">
    <input type="text" id="part_number" class="form-control" placeholder="Команда">
  </div>
  <button type="submit" id="send" class="btn btn-default">Отправить</button> <img id="loader" src="ajax-loader.gif" alt="" />
</form>
</div>
      </div>
</div>

    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="js/bootstrap.min.js"></script>

  </body>
</html>
