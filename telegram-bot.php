<?php

class TelegramBot {
    public function __construct($message) {
        
        if (substr($message, 0, 1) == "/") {
            $message = substr($message, 1);
        }        
        
        // Parsing command:
        if (strstr($message, "spec")) {
            $this->request_type = "specification";
            $this->model = str_replace("spec", "", $message);
        } else {
            $this->request_type = "price";
            $this->model = $message;
        }
        
        $this->request_message = $message;
    }
    
    public function getAnswer() {
        
        if ($this->request_message == "start") {
            
            $answer = "Для получения информации по цене канализации Юнилос и её характеристикам просто спроси меня. Ниже смотри список доступных вопросов: 
/astra3 - цена Юнилос Астра 3; 
/astra3spec - характеристики Юнилос Астра 3; 
Для Юнилос Астра 5 существует 6 команд, 3 для вывода цены на модели и 3 для вывода характеристик.
/astra5 - цена Юнилос Астра 5
/astra5midi - цена Юнилос Астра 5 миди
/astra5long - цена Юнилос Астра 5 лонг
/astra5spec - характеристики Юнилос Астра 5
/astra5midispec - характеристики Юнилос Астра 5 миди
/astra5longspec - характеристики Юнилос Астра 5 миди
Аналогично можно узнать цену и характеристики всех других моделей до Юнилос Астра 300 лонг включительно.
/feedback - оставить отзыв;
/help - если не очень понятно, спроси меня с помощью этой команды, я еще раз всё объясню;
/sale - если хочешь купить станцию со скидкой - напиши мне об этом, я могу помочь.";
            return $answer;
            
        }
        
        if ($this->request_message == "help") {
            $answer = "Спрашивать меня очень просто. Если ты хочешь узнать стоимость какой-то конкретной станции канализации Юнилос Астра, то тебе достаточно написать мне её название. Например, чтобы узнать цену модели Юнилос Астра 8 лонг необходимо написать мне вопрос вида /astra8long. А чтобы увидеть характеристики станции - направляй запрос вида /astra8longspec. И так для всех моделей. Для того, чтобы написать сообщение разработчикам бота или оставить отзыв напиши команду /feedback и в ответном сообщении напиши мне всё, что хочешь, я передам)";
            return $answer;
        }
        
        if ($this->request_message == "sale") {
            $answer = "Хочешь получить значительную скидку на станцию Юнилос? Просто напиши мне письмо  и укажи название станции, которая тебе нужна. Я постараюсь дать лучшую цену.";
            return $answer;
        }
        
        if ($this->request_message == "feedback") {
            $answer = "Простите, эта функция ещё не реализована.";
            return $answer;
        }
        
        $catalog = new Catalog();
        if ($product = $catalog->getProductionByCode($this->model)) {
            
            switch ($this->request_type) {
                case "price":
                    $answer = $product['name']." - ".number_format($product['price'], 0, ".", " ")." руб.\n";
                    break;
                case "specification":
                    $answer = "Характеристики ".$product['name'].":\n\n".$product['specifications']."\n";
                    break;
            }
        } else {
            $answer = "К сожалению, такого товара нет в нашем каталоге.\n";
        }
        
        return $answer;
    }

    private $model;
    private $request_message;
    private $request_type;
}

class Catalog {
    public function __construct() {
        if (($handle = fopen($this->filename, "r")) !== FALSE) {
            while (($data = fgetcsv($handle)) !== FALSE) {  
                $this->production[$data[0]]['name'] = $data[1];
                $this->production[$data[0]]['price'] = $data[2];
                $this->production[$data[0]]['specifications'] = $data[3];
            }
            
            fclose($handle);
        }
    }
    
    public function getProductionByCode($code) {
        if (isset($this->production[$code])) {
            return $this->production[$code];
        }
        return false;
    }

        public function getAllProduction() {
        return $this->production;
    }
    
    private $filename = "catalog.csv";
    private $production;
}

?>
